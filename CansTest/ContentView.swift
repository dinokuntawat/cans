//
//  ContentView.swift
//  CansTest
//
//  Created by Kuntawat surin on 8/12/2566 BE.
//

import SwiftUI

struct ContentView: View {
    
    @State var name: String = ""
    
    var body: some View {
        VStack {
            Image("logo-cans")
                .resizable()
                .scaledToFit()
                .frame(height: 100)
                .padding(.bottom, 40)
            
            TextInputName(labelName: "Input your phone number:", placeHolder: "Enter your Telephone", phoneNumber: $name)
                .padding()
            
            Spacer()
        }
    }
}

struct TextInputName: View {
    @State var labelName: String = ""
    @State var placeHolder: String = ""
    @Binding var phoneNumber: String
    var body: some View {
        VStack(alignment: .leading) {
            Text(labelName)
                .fontWeight(.bold)
                .font(.headline)
                .foregroundColor(Color(red: 9 / 255, green: 118 / 255, blue: 121 / 255))
            
            TextField(placeHolder, text: $phoneNumber)
                .padding()
                .overlay(RoundedRectangle(cornerRadius: 6).strokeBorder(Color(red: 9 / 255, green: 118 / 255, blue: 121 / 255), style: StrokeStyle(lineWidth: 2.0)))
                .keyboardType(.numberPad)
                .onChange(of: phoneNumber) { newValue in
                    phoneNumber = formatPhoneNumber(newValue)
                    let digitCount = countDigits(in: phoneNumber)
                }
        }
        .padding(.horizontal)
    }
    
    func formatPhoneNumber(_ number: String) -> String {
        var formattedNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
        
        if formattedNumber.hasPrefix("0") {
            
            if formattedNumber.count >= 4 {
                formattedNumber.insert("-", at: formattedNumber.index(formattedNumber.startIndex, offsetBy: 3))
            }
            
            if formattedNumber.count >= 8 {
                formattedNumber.insert("-", at: formattedNumber.index(formattedNumber.startIndex, offsetBy: 7))
            }
            
            if countDigits(in: formattedNumber) > 10 {
                formattedNumber = formattedNumber.replacingOccurrences(of: "-", with: "")
            }
            
        } else {
            if formattedNumber.count >= 3 {
                formattedNumber.insert("-", at: formattedNumber.index(formattedNumber.startIndex, offsetBy: 2))
            }
            
            if formattedNumber.count >= 7 {
                formattedNumber.insert("-", at: formattedNumber.index(formattedNumber.startIndex, offsetBy: 6))
            }
            
            if countDigits(in: formattedNumber) > 9 {
                formattedNumber = formattedNumber.replacingOccurrences(of: "-", with: "")
            }
        }

        return formattedNumber
    }
    
    func countDigits(in string: String) -> Int {
        let digitString = string.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        return digitString.count
    }
}

#Preview {
    ContentView()
}
