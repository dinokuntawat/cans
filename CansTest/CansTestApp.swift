//
//  CansTestApp.swift
//  CansTest
//
//  Created by Kuntawat surin on 8/12/2566 BE.
//

import SwiftUI

@main
struct CansTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
